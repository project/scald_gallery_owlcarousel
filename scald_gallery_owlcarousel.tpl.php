<?php
/**
 * @file
 * Theme implementation of the Scald Galleria player
 *
 * Variables:
 * - $options: An array of settings from the player.
 * - $galleria_id: The ID of the gallery instance.
 * - $items: An array of items to be displayed in the gallery.
 */
?>

<div class="scald-gallery-owlcarousle <?php print $galleria_id ?>" style="width: <?php print $options['width']; ?>; height: <?php print $options['height']; ?>">
  <?php $item_group; ?>
  <?php foreach ($items as $item): ?>
    <?php $item_group[]['row'] = $item; ?>
  <?php endforeach; ?>
  <?php print theme('owlcarousel', array('items' => $item_group, 'settings' => $options['owl_settings'])); ?>
</div>
